
#Best explanation of why this coded is needed: https://missionimpossiblecode.io/post/automated-collection-of-diagnostic-system-information-from-systems-you-dont-own/


function ifcmdmissing-instpkg () {
#If you don't need brew, this can be much more compact by removing the relevant code
#Takes a list of Command / Package Pairs
#If command is not on path, installs the package
#detects package managers: apt, yum and brew
  if [[ -n "$(command -v brew)" ]] ; then
    #Detect package manager, brew first because some macOSes have an apt-get stub
    PKGMGR='brew'
  elif [[ -n "$(command -v yum)" ]] ; then
    PKGMGR='yum'
  elif [[ -n "$(command -v apt-get)" ]] ; then
    PKGMGR='apt-get'
  fi
  for cmdpkg in $1 ; do
      IFS=':' read -ra CMDPKGPAIR <<<"${cmdpkg}"
      CMDNAME=${CMDPKGPAIR[0]}
      PKGNAME=${CMDPKGPAIR[1]}
      echo "If command ${CMDNAME} is not on path, the package ${PKGNAME} will be installed."
      if [[ -n "$(command -v ${CMDNAME})" ]]; then
          echo "  '${CMDNAME}' command already present"
      else
          echo "  Missing command '${CMDNAME}'"
          echo "  Installing package '${PKGNAME}' to resolve missing command."
          if [[ $PKGMGR != 'brew' ]]; then
            if [[ $PKGMGR == 'apt-get' ]]; then $PKGMGR update; fi;
            $PKGMGR install -y ${PKGNAME}
          else
            #automatically abstract the difference between brew taps and casks
            if brew info ${PKGNAME} >/dev/null 2>&1; then
              brew install ${PKGNAME} --force
            elif brew cask info ${PKGNAME} >/dev/null 2>&1; then
              brew cask install ${PKGNAME} --force
            else
              echo "  '${PKGNAME}' not found as a formulae or cask"
            fi
          fi
      fi
  done
}

ifcmdmissing-instpkg "jq:jq wget:wget curl:curl"